import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage.filters import convolve

# Laplace operator as convolution kernel.

laplace = np.array([
    [0, 1, 0],
    [1,-4, 1],
    [0, 1, 0]
])

# Right hand sides of the SIR PDEs.

def S_rhs(S, I, N, beta, DS):
    return -beta*S*I/N + DS*convolve(S, laplace)


def I_rhs(S, I, N, beta, gamma, DI):
    return beta*S*I/N - gamma*I + DI*convolve(I, laplace)


def R_rhs(I, R, gamma, DR):
    return gamma*I + DR*convolve(R, laplace)

# Explicit time steps for simulation.

def S_explicit(S, I, N, beta, DS, dt):
    return S + dt * S_rhs(S, I, N, beta, DS)

def I_explicit(S, I, N, beta, gamma, DI, dt):
    return I + dt * I_rhs(S, I, N, beta, gamma, DI)

def R_explicit(I, R, gamma, DR, dt):
    return R + dt * R_rhs(I, R, gamma, DR)

# SIR model with diffusion

def SIR(beta, gamma, DS, DI, DR, S_init, I_init, R_init, sim_time, frames):

    """
    Parameters:
    -----------

    - beta: *float*
        Infection rate.
    - gamma: *float*
        Recovery rate.
    - DS, DR, DI: *array-like*
        Diffusion coefficients for S, I and R populations. Can be space dependent (each value in array corresponds to a point in           space) or single floats.         If an array, it must meet the dimensions of the underlying spatial map.
    - S_init: *array-like*
        Susceptible population at each point in space at the start of the simulation.
    - I_init: *array-like*
        Infected population at each point in space at the start of the simulation.
    - R_init: *array-like*
        Removed population at each point in space at the start of the simulation.
    - sim_time: *float*
        Simulation time.
    - dt: *float*
        Time step size.
    
    Returns:
    --------
    Three separate lists of arrays for the development odf the spatial distribution of S, I, R during the simulation time. Each array corresponds to one frame, which can later be animated.
    """

    # set initial values
    S = S_init
    I = I_init
    R = R_init
    dt = 1e-1
    t = 0

    #output lists
    S_out = list()
    I_out = list()
    R_out = list()

    for T in np.linspace(0, sim_time, frames):
        while t < T:
            N = S + I + R
            S = S_explicit(S, I, N, beta, DS, dt)
            I = I_explicit(S, I, N, beta, gamma, DI, dt)
            R = R_explicit(I, R, gamma, DR, dt)
            t += dt
        S_out.append(S)
        I_out.append(I)
        R_out.append(R)

    return [S_out, I_out, R_out]

