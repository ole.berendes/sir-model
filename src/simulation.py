# imports
import sirmodel
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pandas as pd
from copy import deepcopy
# grid setup
M = 100
N = 100
S = np.full((M,M), N)
I = np.zeros((M,M))
R = np.zeros((M,M))
I[0][0] = 1 # 1 infected person
S[0][0] = 99

# 2nd grid
S2 = np.full((M,M), N)
I2 = np.zeros((M,M))
R2 = np.zeros((M,M))

# diffusion
D = np.full((M, M), 1e-6)

# infection and recovery rate
beta = 5
gamma = 0.02

# simulation time and no. of frames
sim_time = 800
frames = 200

#simulation
dt = 1e-1
t = 0

#output lists
S_frames = list()
I_frames = list()
R_frames = list()

#2nd grid
S2_frames = list()
I2_frames = list()
R2_frames = list()

# empty arrays for analysis data
ana_S = [np.sum(S)]
ana_I = [np.sum(I)]
ana_R = [np.sum(R)]
ana_S2 =[np.sum(S2)]
ana_I2 =[np.sum(I2)]
ana_R2 =[np.sum(R2)]
ana_t = [0]


for T in np.linspace(0, sim_time, frames):
    while t < T:

        # flight traffic between grid 1 and grid 2
        pop_airport1_island1 = deepcopy(I[50, 50])
        pop_airport1_island2 = deepcopy(I2[50, 50])
        pop_airport2_island1 = deepcopy(I[75, 75])
        pop_airport2_island2 = deepcopy(I2[25, 25])
        pop_airport3_island1 = deepcopy(I[76, 76])
        pop_airport3_island2 = deepcopy(I2[51, 51])
        I[50, 50] = pop_airport1_island2
        I2[50, 50] = pop_airport1_island1
        I[75, 75] = pop_airport2_island2
        I2[25, 25] = pop_airport2_island1
        I[76, 76] = pop_airport3_island2
        I2[51, 51] = pop_airport3_island1

        N = S + I + R
        S = sirmodel.S_explicit(S, I, N, beta, D, dt)
        I = sirmodel.I_explicit(S, I, N, beta, gamma, D, dt)
        R = sirmodel.R_explicit(I, R, gamma, D, dt)

        # generate data for analysis
        ana_S.append(np.sum(S))
        ana_I.append(np.sum(I))
        ana_R.append(np.sum(R))


        # 2nd grid
        N2 = S2 + I2 + R2
        S2 = sirmodel.S_explicit(S2, I2, N2, beta, D, dt)
        I2 = sirmodel.I_explicit(S2, I2, N2, beta, gamma, D, dt)
        R2 = sirmodel.R_explicit(I2, R2, gamma, D, dt)

        ana_S2.append(np.sum(S2))
        ana_I2.append(np.sum(I2))
        ana_R2.append(np.sum(R2))

        t += dt
        ana_t.append(t)

    S_frames.append(S)
    I_frames.append(I)
    R_frames.append(R)

    # 2nd grid
    S2_frames.append(S2)
    I2_frames.append(I2)
    R2_frames.append(R2)

# generate animation
ims = []
fig, (ax1, ax2) = plt.subplots(2, 1)

for frame1, frame2 in zip(I_frames, I2_frames):
    im = [ax1.imshow(frame1, animated=True, vmin=0, vmax=100), ax2.imshow(frame2, animated=True, vmin=0, vmax=100)]
    ims.append(im)

anim = animation.ArtistAnimation(fig, ims, interval=50, blit=True, repeat_delay=100)
writergif = animation.PillowWriter(fps=10)
anim.save('output/animation.gif', writer=writergif)
# plt.show()

# generate dat frame with analysis data
colnames = ["time", "S", "I", "R", "S2", "I2", "R2"]
dfAna = pd.DataFrame(np.transpose([ana_t, ana_S, ana_I, ana_R, ana_S2, ana_I2, ana_R2]), columns=colnames)
dfAna.to_csv("output/data/ana_data.csv", index=False)
# print(np.transpose([ana_t, ana_S, ana_I, ana_R, ana_S2, ana_I2, ana_R2]))